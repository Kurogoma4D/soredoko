import 'package:flutter/material.dart';
import 'package:soredoko/model.dart';
import 'package:soredoko/suggestCard.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:async';
import 'dart:convert';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'それドコ？',
      theme: ThemeData(
          primarySwatch: Colors.indigo,
          cardColor: Color.fromRGBO(255, 251, 230, 1),
          fontFamily: 'MgenPlus-r',
          textTheme: TextTheme(
              title: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 24,
          ))),
      home: MyHomePage(title: 'それドコ？'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  //取得した部屋一覧
  List<Room> _rooms = [
    //Room.fromJson(json.decode('{"name":"ほげ","building":"ほげ","floor":"2"}'))
  ];

  //入力部分
  Widget _buildInput() {
    return Container(
      color: Colors.white,
      child: Padding(
        padding: EdgeInsets.fromLTRB(8, 0, 8, 8),
        child: TextField(
          decoration: InputDecoration(
            labelText: "部屋名",
            icon: Icon(Icons.search),
          ),
          onSubmitted: (inputString) {
            if (inputString.length >= 1) {
              _getRooms(inputString).then((rooms) {
                setState(() {
                  _rooms = rooms;
                  _rooms.sort((a, b) => a.name.compareTo(b.name));
                });
              });
            }
          },
        ),
      ),
    );
  }

  //APIコール
  Future<List<Room>> _getRooms(String word) async {
    final String serverURL = "http://192.168.0.21:8080/rooms";
    final response = await http.get(serverURL + '?q=' + word);
    List<Room> list = [];
    if (response.statusCode == 200) {
      Map<String, dynamic> decoded = json.decode(response.body);
      for (var r in decoded['rooms']) {
        list.add(Room.fromJson(r));
      }
      return list;
    } else {
      Fluttertoast.showToast(
        msg: "部屋が見つかりません。",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
      );
      return list;
      //throw Exception("Not found.");
    }
  }

  //リスト
  Widget _buildList() {
    return ListView.builder(
      shrinkWrap: true,
      reverse: true,
      itemBuilder: (BuildContext context, int index) {
        final Room r = _rooms[index];
        return SuggestCard(roomData: r, id: index.toString());
      },
      itemCount: _rooms.length,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        backgroundColor: Color.fromRGBO(11, 37, 137, 1),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Flexible(
            //listview of fetched data
            child: _buildList(),
          ),
          Divider(height: 1),
          //input
          _buildInput(),
        ],
      ),
    );
  }
}
