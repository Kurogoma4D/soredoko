import 'package:flutter/material.dart';
import 'package:soredoko/model.dart';

class DetailScreen extends StatelessWidget {
  final Room roomData;
  final String id;

  DetailScreen({this.roomData, this.id});

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Container(
          margin: EdgeInsets.fromLTRB(0, 24, 0, 0),
          color: Color.fromRGBO(255, 251, 230, 1),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                height: 128,
                child: Stack(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(16),
                      child: IconButton(
                        tooltip: "close",
                        icon: Icon(Icons.close, size: 24),
                        onPressed: () => Navigator.of(context).pop(),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(72, 0, 0, 28),
                      child: Align(
                        alignment: Alignment.bottomLeft,
                        child: Hero(
                          tag: "room$id",
                          child: Text(
                            roomData.name,
                            style: TextStyle(
                                color: Color.fromRGBO(11, 37, 137, 1),
                                fontWeight: FontWeight.bold,
                                fontSize: 36),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              //body
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(left: 72),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'は...',
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w500),
                      ),
                      SizedBox(height: 16),
                      Text(
                        'ココ！',
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                            color: Colors.redAccent),
                      ),
                      Container(
                          margin: EdgeInsets.fromLTRB(0, 0, 8, 8),
                          padding: EdgeInsets.all(8),
                          decoration: BoxDecoration(
                            border:
                                Border.all(color: Colors.redAccent, width: 2),
                            borderRadius: BorderRadius.circular(4),
                          ),
                          child: Text(
                            roomData.building + "\n" + roomData.floor,
                            style: TextStyle(
                                fontSize: 36, fontWeight: FontWeight.w500),
                            softWrap: true,
                          ))
                    ],
                  ),
                ),
              ),
            ],
          )),
    );
  }
}
