class Room {
  final String name;
  final String building;
  final String floor;

  Room.fromJson(Map<String, dynamic> json)
    : name = json['name'],
    building = json['building'],
    floor = json['floor'];
}