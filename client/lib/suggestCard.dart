import 'package:flutter/material.dart';
import 'package:soredoko/model.dart';
import 'package:soredoko/detail.dart';

class SuggestCard extends StatelessWidget {
  final Room roomData;
  final String id;

  SuggestCard({this.roomData, this.id});

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.fromLTRB(8, 0, 8, 8),
      child: InkWell(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  fullscreenDialog: true,
                  builder: (context) =>
                      DetailScreen(roomData: roomData, id: id)));
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.fromLTRB(8, 12, 0, 0),
              child: Hero(
                tag: "room$id",
                child: Container(
                  width: double.infinity,
                  child: Text(
                    roomData.name,
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      color: Color.fromRGBO(11, 37, 137, 1),
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(8, 4, 0, 16),
              child: Text(
                roomData.building,
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontWeight: FontWeight.normal,
                  fontSize: 12,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
