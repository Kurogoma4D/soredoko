package ParseData

type JsonData struct {
	Rooms []Room `json:"rooms"`
}

type Room struct {
	Name     string `json:"name"`
	Building string `json:"building"`
	Floor    string `json:"floor"`
}
