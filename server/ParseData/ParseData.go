package ParseData

import (
	"fmt"

	"github.com/tealeg/xlsx"
)

func FromExcel(fileName string) JsonData {
	xlFile, err := xlsx.OpenFile(fileName)

	if err != nil {
		fmt.Println("An error occured.")
	}

	returnData := JsonData{Rooms: []Room{}}

	for _, sheet := range xlFile.Sheet {
		for _, row := range sheet.Rows {
			level := ""
			for i, cell := range row.Cells {
				roomName := cell.String()
				if roomName == "" {
					continue
				}

				if i == 0 {
					level = roomName
				} else {
					returnData.Rooms = append(returnData.Rooms, Room{
						Name:     roomName,
						Building: sheet.Name,
						Floor:    level,
					})
				}
			}
		}
	}

	return returnData
}
