package main

import (
	"./ParseData"
	"./handler"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func main() {
	e := echo.New()
	roomData := ParseData.FromExcel("./assets/data.xlsx")

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.GET("/hello", handler.MainPage())
	e.GET("/rooms", handler.NarrowedRoom(roomData))

	e.Start(":8080")
}
