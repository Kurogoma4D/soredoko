package handler

import (
	"net/http"
	"regexp"

	"../ParseData"

	"github.com/labstack/echo"
)

func MainPage() echo.HandlerFunc {
	return func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello World")
	}
}

func NarrowedRoom(roomData ParseData.JsonData) echo.HandlerFunc {
	return func(c echo.Context) error {
		var narrowedData = ParseData.JsonData{Rooms: []ParseData.Room{}}

		// クエリストリングを取得
		q := c.QueryParam("q")

		// クエリストリングをデコード（いらないかも）
		//q, _ = url.QueryUnescape(q)

		// roomDataからクエリストリングの文字列に前方一致するものを検索
		r := regexp.MustCompile("^" + q)
		for _, d := range roomData.Rooms {
			if r.MatchString(d.Name) {
				narrowedData.Rooms = append(narrowedData.Rooms, d)
			}
		}

		// JSONオブジェクトを返す
		//encodedData, _ := json.Marshal(narrowedData)
		//return c.JSONPretty(http.StatusOK, narrowedData, "  ")
		if len(narrowedData.Rooms) == 0 {
			return c.NoContent(http.StatusBadRequest)
		} else {
			return c.JSON(http.StatusOK, narrowedData)
		}
	}
}
